# Práctica 1 OpenMP - Multiplicación de Matrices

El objetivo de la práctica será medir el rendimiento secuencial del algoritmo clásico de multiplicación de matrices y paralelizarlo. 

```
float a[TAM][TAM];
    float b[TAM][TAM];
    float c[TAM][TAM];
    for (i=0;i<TAM;i++)
       for (j=0;j<TAM;j++) {
          c[i][j]=0;
          for (k=0;k<TAM;k++)
             c[i][j]= c[i][j]+a[i][k]*b[k][j];
```

Se obtendrá la media de 3 tiempos de ejecución en la máquina de cómputo paralelo [geopar][geopar] y generaremos una curva de *speedup* para 1, 4, 8 y 12 hilos de ejecución.

## Ejecución

### Secuencial

#### Local

```
$ make sequential
$ ./mult_secuencial
```

#### Geopar

```
$ client -u usuario -q sequential mult_sec.c
```

### Paralela

#### Local

```
$ make all
$ ./mult
```

#### Geopar

La opción `-t` de [client][client] se usa en luegar de la sentencia `omp_set_num_threads()` para especificar el número de hilos OpenMP.

```
$ client -u usuario -q openmp -t 4 mult.c 
```

## Resultados:

| Request         | Time     | Average     | Speedup     |
|-----------------|----------|-------------|-------------|
| Secuencial      |          | 8.756123    | 1           |
| 493             | 8.756318 |             |             |
| 494             | 8.755953 |             |             |
| 495             | 8.756098 |             |             |
| 1 procesador    |          | 8.757796    | 0.99980897  |
| 496             | 8.755999 |             |             |
| 497             | 8.757964 |             |             |
| 498             | 8.759425 |             |             |
| 4 procesadores  |          | 2.225286667 | 3.934829221 |
| 499             | 2.22864  |             |             |
| 500             | 2.223034 |             |             |
| 501             | 2.224186 |             |             |
| 8 procesadores  |          | 1.12254     | 7.80027705  |
| 502             | 1.122487 |             |             |
| 503             | 1.122157 |             |             |
| 504             | 1.122976 |             |             |
| 12 procesadores |          | 0.795783667 | 11.00314491 |
| 505             | 0.79145  |             |             |
| 506             | 0.792642 |             |             |
| 507             | 0.803259 |             |             |

### Generación de la gráfica de *speedup*

```
$ gnuplot script.gnp > speedup-XX.eps
```

![grafica](http://i.imgur.com/RCSL5IC.jpg)

## Dependencias:

* [**client**][client]
* [gcc-4.9](https://gcc.gnu.org/gcc-4.9/)

[geopar]: http://geopar.infor.uva.es:8080/
[client]:http://geopar.infor.uva.es:8080/client