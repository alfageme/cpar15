
#include<stdio.h>
#include<stdlib.h>
#include<omp.h>
#include"cputils.h"

#define TAM 1000

// Definimos las tres matrices
// (Hay que definirlas como globales porque desbordan la pila del proceso)

float a[TAM][TAM];
float b[TAM][TAM];
float c[TAM][TAM];

int main() {

   int i, j, k; // Indices de bucles
   int checksum=0;
   double inicio, fin;

   // Inicializaciones de A y B
   for (i=0;i<TAM;i++)
      for (j=0;j<TAM;j++)
         a[i][j]=1;

   for (i=0;i<TAM;i++)
      for (j=0;j<TAM;j++)
         b[i][j]=2;

   // Producto
   inicio=cp_Wtime();
   for (i=0;i<TAM;i++) {
      for (j=0;j<TAM;j++) {
         c[i][j]=0;
         for (k=0;k<TAM;k++)
            c[i][j] += a[i][k]*b[k][j];
      }
   }
   fin=cp_Wtime();

   // Se muestra el resultado por pantalla
#ifdef PRINT
   for (i=0;i<TAM;i++) {
      for (j=0;j<TAM;j++) 
         printf("\t%f",c[i][j]);
      printf("\n");
   }
#endif 

   // Se calcula y muestra el checksum
   
   for (i=0;i<TAM;i++) 
      for (j=0;j<TAM;j++) 
         checksum = (checksum + (int) c[i][j]) % 999999;

   printf("Checksum: %d\n",checksum);
   printf("Time: %lf\n",(fin-inicio));
   exit(EXIT_SUCCESS);
}
