/**
 * Computación Paralela
 *
 * Calcula el máximo de cada elemento del array de valores generado en cada proceso.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#include "cputils.h"
#include "cputils2.h"

#define ARRAY_SIZE 10

/**
 * Main function
 */
int main( int argc, char *argv[] ){

	// 1. Variables
	int rank, size;


	// 2. Iniciar MPI
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	// 3. Check number of processors
	if(size < 2){
		if(rank == 0) fprintf(stderr,"I need 2 processors at least\n");
		MPI_Finalize();
		return EXIT_FAILURE;
	}

	// 4. Creación de un array.
	int * data = malloc((size_t) ARRAY_SIZE * sizeof(int));
	int i;

	// 5. Inicialización del array con valores aleatorios.
	for(i = 0; i<ARRAY_SIZE; i++){
		data[i] = cp_rand(100,1000);
	}
	printf("[%2d] I have: %s\n",rank, cp_array2str(data,ARRAY_SIZE) );

	// 6. Creación de otro array para los máximos.
	int * data_max = malloc((size_t) ARRAY_SIZE * sizeof(int));

	// 7. Redución de cada elemento de los arrays en el proceso 0.
	MPI_Reduce(data, data_max, ARRAY_SIZE, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);

	// 8. 0 imprime el array resultante.
	if(rank == 0){
		printf("[%2d] Max is: %s\n",rank, cp_array2str(data_max,ARRAY_SIZE));
	}

	MPI_Finalize();
	return EXIT_SUCCESS;
}
