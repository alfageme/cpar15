/**
 * Computación Paralela - Ejemplo de Scatterv
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#include "cputils.h"
#include "cputils2.h"

/**
 * Main
 */
int main( int argc, char *argv[] ){

	// 1. Variables
	int rank, size;




	// 2. Iniciar MPI
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	if(size != 3){
		if(rank==0) fprintf(stderr,"Este ejemplo usa 3 procesos\n");
		MPI_Abort(MPI_COMM_WORLD,1234);
	}




	// 3. Declaración de variables
	int array_size = 20;
	int * array = NULL;
	int * array_local = NULL;



	// 4. El proceso 0 inicia array
	if(rank==0){
		array = malloc(sizeof(int)*(size_t)array_size);
		int i;		
		for(i=0; i<array_size; i++) array[i] = cp_rand(0,99);

		printf("Soy 0 y tengo el array completo: %s\n", cp_array2str(array,array_size));
	} else {

		printf("Soy %d, mi variable array es NULL pero se que 0 tiene un array de tamaño %d\n",rank,array_size);

	}


	// Esto lo pongo para separar la salida de una parte del ejemplo.
	// No pongáis sleeps en un código de verdad.
	{
		cp_msleep(250);
		if(rank==0) printf("\n");
		cp_msleep(250);
	}



	// 5. Los procesos determinan la partición (número de elementos y desplazamiento)
	// Atención: Todos tienen todo, esto no está dentro de ningún if.
	// Atención 2: Esto está hard-coded para 3 procesos,
	// en la práctica habría que calcularlo en función:
	// del tamaño de la cadena y el número de procesos.
	// También habría que hacer el solapamiento.
	int tam[3] = {7,7,6};
	int disp[3] = {0,7,14};

	printf("Tamaños: %s\n", cp_array2str(tam,size));
	printf("Desplazamientos: %s\n", cp_array2str(disp,size));


	// 6. Cada proceso reserva su parte
	int array_size_local = tam[rank]; // Saco el tamaño del array tam
	array_local = malloc(sizeof(int)*(size_t)array_size_local);


	
	// 7. Scatter
	MPI_Scatterv(
		// Parámetros relevantes para 0
		array,tam,disp,MPI_INT,
		// Parámetros relevantes para todos (0 incluido)
		array_local,array_size_local,MPI_INT,
		// Seleccionamos el root y comunicador
		0, MPI_COMM_WORLD
	);


	// Pausa
	{
		cp_msleep(250);
		if(rank==0) printf("Scatterv ... \n\n");
		cp_msleep(250);
	}


	// 8. Cada uno imprime su parte
	printf("Soy %d y tengo el array parcial: %s\n", rank, cp_array2str(array_local,array_size_local));


	// 9. Frees
	// Todos tienen array_local, 0 solo tiene 
	free(array_local);
	if(rank==0)free(array);


	MPI_Finalize();
	return EXIT_SUCCESS;
}
