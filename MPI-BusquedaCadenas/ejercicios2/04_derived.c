/**
 * Computación Paralela
 * Creación de tipos MPI derivados.
 * 
 * @author Javier Fresno
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#include "cputils.h"
#include "cputils2.h"

/**
 * Punto con tres coordenadas.
 */
typedef int Point[3];

/**
 * Struct con diferentes campos.
 */
typedef struct {
	Point point;
	double value;
	char type;
} Data;


/**
 * Main function
 */
int main( int argc, char *argv[] ){

	// 1. Variables
	int rank, size;
	int tag = 0;

	// 2. Iniciar MPI
	MPI_Init( &argc, &argv );
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	MPI_Comm_size( MPI_COMM_WORLD, &size );

	// 2.1 Check number of processors
	if(size != 2){
		if(rank == 0) fprintf(stderr,"I need 2 processors\n");
		MPI_Finalize();
		return EXIT_FAILURE;
	}

	// 3. Creación del tipo derivado MPI_Point
	MPI_Datatype MPI_Point;
	MPI_Type_contiguous(3,MPI_INT, &MPI_Point);
	MPI_Type_commit(&MPI_Point);

	// 4. Creación del tipo derivado MPI_Data
	MPI_Datatype MPI_Data;
	Data data;
	
	// 4.1. Direcciones de los campos
	MPI_Aint address_data;
	MPI_Aint address_point;
	MPI_Aint address_value;
	MPI_Aint address_type;

	MPI_Get_address(&data, &address_data);
	MPI_Get_address(&data.point, &address_point);
	MPI_Get_address(&data.value, &address_value);
	MPI_Get_address(&data.type, &address_type);

	// 4.2 Calculo de los desplazamientos
	MPI_Aint displ_point = address_point - address_data;
	MPI_Aint displ_value = address_value - address_data;
	MPI_Aint displ_type = address_type - address_data;

	// Mostramos la información
	if(rank == 0){

		printf("sizeof(int)    = %d\n", (int) sizeof(int) );
		printf("sizeof(Point)  = %d\n", (int) sizeof(Point) );
		printf("sizeof(double) = %d\n", (int) sizeof(double) );
		printf("sizeof(char)   = %d\n", (int) sizeof(char) );
		printf("sizeof(Data)   = %d\n", (int) sizeof(Data) );

		printf("displ(point)   = %d\n", (int) displ_point);
		printf("displ(value)   = %d\n", (int) displ_value);
		printf("displ(type)    = %d\n", (int) displ_type);
	}


	// 4.3 Creación del tipo
	int count = 3;
	int array_of_blocklengths[3] = {1,1,1};
	MPI_Aint array_of_displacements[3] = {displ_point,displ_value,displ_type};
	MPI_Datatype array_of_types[3] = {MPI_Point, MPI_DOUBLE, MPI_CHAR};
	MPI_Type_create_struct(count, array_of_blocklengths, array_of_displacements, array_of_types, &MPI_Data);
	MPI_Type_commit(&MPI_Data);


	// 5. Comunicación
	if(rank == 0){

		// 5.a. Iniciamos el struct y lo enviamos
		data.point[0] = 34;
		data.point[1] = 86;
		data.point[2] = 12;
		data.value = 3.14;
		data.type = 'K';

		printf("Sending Data  = { Point %s, Value %5.3f, type %c }\n",cp_array2str(data.point, 3),data.value,data.type);
		MPI_Send( &data, 1, MPI_Data, 1, tag, MPI_COMM_WORLD );
		
	} else if (rank == 1){

		// 5.b. Recibimos el struct
		MPI_Recv( &data, 1, MPI_Data, 0, tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		printf("Received Data = { Point %s, Value %5.3f, type %c }\n",cp_array2str(data.point, 3),data.value,data.type);
	}

	// 6. Liberar los tipos
	MPI_Type_free(&MPI_Point);
	MPI_Type_free(&MPI_Data);


	// 7. Finalizar MPI
	MPI_Finalize();
	return EXIT_SUCCESS;
}
