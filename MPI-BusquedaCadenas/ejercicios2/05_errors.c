/**
 * Computación Paralela
 *
 * MPI Error handling
 *
 * @author Javier Fresno
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main( int argc, char *argv[] ){

	// 1. Variables
    int rank, size;
	int data;

	// 2. Init MPI
	MPI_Init( &argc, &argv );
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	MPI_Comm_size( MPI_COMM_WORLD, &size );

	// 3. Set the return handler to the main communicator
	MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);
	
	// 4. Send a Message to a fake processor rank
	int ierr = MPI_Send( &data, 1, MPI_INT, 100, 0, MPI_COMM_WORLD );

	// 5. Check the error
	if (ierr != MPI_SUCCESS) {
		char errstr[MPI_MAX_ERROR_STRING];
		int lerrstr;
		int errcls;

		MPI_Error_class(ierr,&errcls);
		MPI_Error_string(ierr, errstr, &lerrstr);

		fprintf(stderr,"Error code: %d, error class %d: %s\n",ierr,errcls,errstr);
	}

	// 6. Finalización
	MPI_Finalize();
	return 0;
}
