/*
 * busquedaP.c
 * @author Samuel Alfageme Sainz, Pablo Garc�a Sanz - Grupo 35 : Computaci�n Paralela
 * @version 0.1
 * @date 2015/03/XX
 */
#include <stdio.h>
#include <stdlib.h>
#include "cputils.h"

/*
 * FUNCION: BUSQUEDA DE SECUENCIAS
 * S: Secuencia de busqueda
 * B: Secuencia base
 * sizeS: Longitud de la secuencia de busqueda
 * sizeB: Longitud de la secuencia base
 * return Indice en B de la primera aparici�n de S, o -1 si no aparece S en B
 */
int busqueda( char *S, int sizeS, char *B, int sizeB ) {
	int result = -1; // Resultado de la busqueda

	/* 1. PARA CADA POSICION DE COMIENZO EN B (ultima posible sizeB-sizeS) */ 
	int startB;
	omp_set_num_threads(14);
	#pragma omp parallel for shared(sizeS,sizeB,S,B,result) \
	 private(startB) default(none) schedule(static)
	for( startB = 0; startB <= sizeB-sizeS ; startB++ ) {
		if(result == -1){
			int ind;
			/* 2. PARA CADA POSICION DE S */ 
			for( ind = 0; ind<sizeS; ind++ ) {
				/* 2.1. CARACTER NO COINCIDENTE, PASAR A SIGUIENTE POSICION DE B */
				if ( S[ind] != B[startB+ind] ) break;
			}
			/* 3. COMPROBAR CONDICION DE SALIDA DEL BUCLE */
			if ( ind == sizeS ) {
				#pragma omp critical
				{
					result = startB;
				}
			}
		}
	}
	return result;	
}




/*
 * MAIN: LEER FICHEROS EN MEMORIA Y LLAMAR A BUSQUEDA DE SECUENCIAS
 */

int main(int argc, char *argv[]) {

	double inicio,fin,tiempoAcumulado=0; // Contadores de tiempo.
	size_t sizeB, *sizeS; // Para almacenar los tama�os de los ficheros.
	char *B, **S; // Para almacenar las cadenas.
	int i; 

	/* COMPROBAR ARGUMENTOS */
	if ( argc < 3 ) {
		fprintf( stderr, "\nFormato: %s <cad-B> <cad-S0> {<cad-Si>} \n\n", argv[0] );
		exit( EXIT_FAILURE );
	}

	// Creamos estructuras de datos para trabajar con las cadenas Si pasadas como argumentos.
	// Creamos un vector de tama�o variable para guardar los tama�os de todas las cadenas Si.
	sizeS = (size_t*) malloc( (size_t) (argc-2)*sizeof(size_t));
	// Creamos un vector de punteros para guardar las propias cadenas Si.
	S = (char**) malloc( (size_t) (argc-2)*sizeof(char*));

	// Recuperamos el tama�o de la cadena larga.
	if ( ! cp_FileSize(argv[1],&sizeB) ) {
		fprintf( stderr, "Error: Imposible leer fichero %s\n", argv[1] );
		exit( EXIT_FAILURE );
	}

	// Reservamos memoria para cargar el fichero con la cadena larga.
	B = (char *) malloc( sizeB );
	if ( B == NULL ) {
		fprintf( stderr, "Error: Imposible reservar memoria para %d bytes\n", (int) sizeB );
		exit( EXIT_FAILURE );
	}

	// Cargamos el fichero con la cadena larga.
	if ( ! cp_FileRead(argv[1],B) ) {
		fprintf( stderr, "Error: En lectura de %s\n", argv[1] );
		exit( EXIT_FAILURE );
	}

	// Repetimos el mismo proceso con los (argc-2) ficheros que contienen las cadenas cortas 
	for (i=0;i<(argc-2);i++) {

		// Recuperamos el tama�o del fichero con la i-esima cadena corta.
		if ( ! cp_FileSize(argv[i+2],&(sizeS[i])) ) {
			fprintf( stderr, "Error: Imposible leer fichero %s\n", argv[i+2] );
			exit( EXIT_FAILURE );
		}

		// Reservamos memoria para la i-esima cadena corta.
		S[i] = (char *)malloc( sizeS[i] );
		if ( S[i] == NULL ) {
			fprintf( stderr, "Error: Imposible reservar memoria para %d bytes\n", (int) sizeS[i] );
			exit( EXIT_FAILURE );
		}

		// Leemos la i-esima cadena corta.
		if ( ! cp_FileRead(argv[i+2],S[i]) ) {
			fprintf( stderr, "Error: En lectura de %s\n", argv[i+2] );
			exit( EXIT_FAILURE );
		}

	}

#ifdef DEBUG
	printf("DEBUG Tam fichero grande: %d\n", (int) sizeB);
	for (i=0;i<(argc-2);i++) 
		printf("DEBUG Tam fichero #%d (%s): %d\n", i, argv[i+2], (int) sizeS[i]);
#endif

#ifdef PRINTSTRINGS
	printf("DEGUB B={" );
	for ( i = 0; i<sizeB; i++) printf( "%c", B[i] );
	printf("}\n" );
	for (i=0;i<(argc-2);i++){
		printf("DEGUB S={" );
		int j;
		for ( j = 0; j<sizeS[i]; j++) printf( "%c", S[i][j] );
		printf("}\n" );
	}
#endif

	/* 2. BUSQUEDA */
	for (i=0;i<(argc-2);i++) {
		inicio=cp_Wtime();
		int result = busqueda( S[i], (int) sizeS[i], B, (int) sizeB );
		fin=cp_Wtime();
		tiempoAcumulado+=fin-inicio;
		printf("Result: %d\n", result);
	}
	printf("Time: %lf\n",tiempoAcumulado);

	/* 3. FREEs */
	free(B);
	for (i=0;i<(argc-2);i++) 
		free(S[i]);
	free(sizeS);


	/* 4. FIN */
	return 0;
}
