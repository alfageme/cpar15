/**
 * Computación Paralela
 *
 * Deadlock 2º tipo
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

int main( int argc, char *argv[] ){

	// 1. Variables
	int rank, size;
	int data = 123;
	int tag = 0;
	MPI_Status stat;

	// 2. Iniciar MPI
	MPI_Init( &argc, &argv );
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	MPI_Comm_size( MPI_COMM_WORLD, &size );

	// Leer argumentos
	if(argc != 2){
		printf("Usage: %s size\n",argv[0]);
	}

	// 3. Reservar memoria para los mensajes de envío y recepción.
	int msgsize = atoi(argv[1]);
	printf("Msgsize %d\n",msgsize);
	int * msgsend = malloc((size_t) msgsize * sizeof(int) );
	int * msgrecv = malloc((size_t) msgsize * sizeof(int) );

	int i;
	for(i=0; i<msgsize; i++) msgsend[i] = data;


	// 4. Calcular el destino
	int dest = (rank + 1) % size;

	// 5. Envío de la información
	printf( "Process %d sending to %d\n", rank, dest);
	MPI_Send( msgsend, msgsize, MPI_INT,           dest, tag, MPI_COMM_WORLD );

	// 6. Recepción
	MPI_Recv( msgrecv, msgsize, MPI_INT, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &stat );
	printf( "Process %d received from %d\n", rank, stat.MPI_SOURCE);

	// 7. Finalizar MPI.
	MPI_Finalize();
	return 0;
}
