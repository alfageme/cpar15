/**
 * Computación Paralela
 *
 * Completa el programa:
 * Todos los procesos envian un string aleatorio al proceso con rank 0,
 * el proceso 0 los recibe e imprime por pantalla.
 * 
 * @author Javier Fresno
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>


// Utilidades para la asignatura
#include "cputils.h"
#include "cputils2.h"

#define NUMBERS 10

int main( int argc, char *argv[] ){

	// 1. Variables
	int rank, size;
	int tag = 0;

	// 2. Init MPI
	MPI_Init( &argc, &argv );
	MPI_Comm_rank( MPI_COMM_WORLD, &rank );
	MPI_Comm_size( MPI_COMM_WORLD, &size );

	// Check number of processes.
	if(size < 2){
		if(rank == 0) fprintf(stderr,"I need at least 2 processor\n");
		MPI_Finalize();
		return EXIT_FAILURE;
	}

	// 3.a. Los procesos envían un dato a 0.
	if(rank != 0){

		// Cada proceso recibe un srtring al azar.
		int len = cp_rand(5,NUMBERS);
		int * numbers = malloc( (size_t) len * sizeof(int) );
		int i;
		for(i=0; i<len; i++) numbers[i] = cp_rand(0,99);

		// Envío del dato al proceso 0.
	 	printf( "Process %d sending %s\n", rank, cp_array2str(numbers,len));
		MPI_Send( numbers, len, MPI_INT, 0, tag, MPI_COMM_WORLD );

	// 3.b. El proceso 0 recibe size-1 mensajes.
	} else {

		int i;
		MPI_Status stat;

		// Bucle para recibir los mensajes
		for(i=1; i<size; i++){

			// Selección del origen. Dos opciones:
			// source = i:              Los mensajes se reciben en orden.
			// source = MPI_ANY_SOURCE: Los mensajes se reciben según llegan (+ eficiente).		
			int source = MPI_ANY_SOURCE;
			int numbers[NUMBERS];

			// Recepción del mensaje
			MPI_Recv( numbers, NUMBERS, MPI_INT, source, tag, MPI_COMM_WORLD, &stat );

			// Obtener el tamaño del mensaje.
			int len;
			MPI_Get_count(&stat, MPI_INT, &len);
			printf( "I'm, I've received %s from %d\n", cp_array2str(numbers,len), stat.MPI_SOURCE );
		}	
	}
	
	// 4. Mensaje de finalización.
	printf("Process %d has finished.\n",rank);

	MPI_Finalize();
	return 0;
}
