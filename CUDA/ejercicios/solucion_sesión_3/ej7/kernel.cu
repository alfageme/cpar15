/*****************************************************************
Función del device para la suma de vectores (primer Kernel)
*****************************************************************/
__global__ void gpuFunc_vecAdd1(int *A, int *B, int *C)
{
	/*Identificaciones necesarios*/
	int IDX_Thread		=	threadIdx.x;				//Identificación del hilo en la dimension x
	int IDX_block		=	blockIdx.x;				//Identificación del bloque en la dimension x
	int thread_per_block	=	blockDim.x*blockDim.y*blockDim.z;	//Número de hilos por bloque

	/*Fórmula para calcular la posición*/	
	int position		=	thread_per_block * IDX_block + IDX_Thread;	//Posición del vector dependiendo del hilo y del bloque 

	/*Hacemos la suma correspondiente*/
	C[position]=	A[position] + B[position];
	
} // gpuFunc_vecAdd1


/*****************************************************************
Función del device para la suma de vectores (segundo Kernel)
*****************************************************************/
__global__ void gpuFunc_vecAdd2(int *A, int *B, int *C)
{
	/*Identificaciones necesarios*/
	int IDX_Thread		=	threadIdx.x;				//Identificación del hilo en la dimension x
	int IDY_Thread		=	threadIdx.y;				//Identificación del hilo en la dimension y
	int IDX_block		=	blockIdx.x;				//Identificación del bloque en la dimension x
	int IDY_block		=	blockIdx.y;				//Identificación del bloque en la dimension y
	int shapeGrid_X		=	gridDim.x;				//Números del bloques en la dimensión x
	int thread_per_block	=	blockDim.x*blockDim.y*blockDim.z;	//Número de hilos por bloque

	/*Fórmula para calcular la posición*/	//Posición del vector dependiendo del hilo y del bloque 
	int position		=	
			IDY_block*shapeGrid_X*thread_per_block + IDX_block*thread_per_block + IDY_Thread*blockDim.x + IDX_Thread;

	/*Hacemos la suma correspondiente*/
	C[position]=	A[position] + B[position];

} // gpuFunc_vecAdd2
