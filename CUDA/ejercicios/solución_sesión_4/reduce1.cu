#include <stdio.h>
#include <cutil.h>

#define NUM_THREADS_PER_BLOCK       512
#define NUM_BLOCKS                  4
#define NUM_THREADS                 (NUM_THREADS_PER_BLOCK * NUM_BLOCKS)

static int* d_Result;
extern "C" void reduceAllocate(){ CUDA_SAFE_CALL(cudaMalloc((void**)&d_Result, NUM_BLOCKS * sizeof(int))); }
extern "C" void reduceFree(){     CUDA_SAFE_CALL(cudaFree(d_Result));}


__global__ void reduce_kernel(const int*, int*);

extern "C" int* reduce(const int* values, unsigned int numValues){
    // Execution configuration
    int numThreadsPerBlock = NUM_THREADS_PER_BLOCK;
    int numBlocks = NUM_BLOCKS;

    // The first pass reduces the input array to an array of size equal to the total number of blocks in the grid
    int sharedMemorySize = numThreadsPerBlock * sizeof(int);
    reduce_kernel<<<numBlocks, numThreadsPerBlock, sharedMemorySize>>>(values, d_Result);
    CUT_CHECK_ERROR("Kernel execution failed");
    
    // The second pass launches only one block to perform the final reduction
    numThreadsPerBlock = numBlocks;
    numBlocks = 1;
    sharedMemorySize = numThreadsPerBlock * sizeof(int);
    reduce_kernel<<<numBlocks, numThreadsPerBlock, sharedMemorySize>>>(d_Result, d_Result);
    
    return d_Result;
}

__global__ void reduce_kernel(const int* g_idata, int* g_odata){

    extern __shared__ int sdata[];

    // each thread loads one element from global to shared mem
    unsigned int tid = threadIdx.x;
    unsigned int igl = blockIdx.x * blockDim.x + tid;
    sdata[tid] = g_idata[igl];
    __syncthreads();
    
    // do reduction in shared memory
    for(unsigned int s = 1; s < blockDim.x; s *= 2) {
        // Check if the thread is active during this loop iteration
        if (tid % (2 * s) == 0){
            // Accumulate one element from sdata into another
            sdata[tid] += sdata[tid + s];
	}
        __syncthreads();
    }
    
    // Thread 0 of each block writes the final result of the reduction to device memory
    if (tid == 0)
        g_odata[blockIdx.x] = sdata[0];
}
