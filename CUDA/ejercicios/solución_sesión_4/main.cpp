#include <stdio.h>
#include <stdlib.h>
#include <cuda_runtime.h>
#include <cutil.h>

extern "C" void reduceAllocate();
extern "C" void reduceFree();
extern "C" int* reduce(const int*, unsigned int);

int reduceHost(const int*, unsigned int);

int main(int argc, char **argv){

    // Number of values to be added up together
    unsigned int numValues = 2048;

    // Size in memory
    unsigned int size = numValues * sizeof(int);

    // Allocate host memory for the values
    int* h_values;
    CUT_SAFE_MALLOC(h_values = (int*)malloc(size));

    // Initialize with random data
    for (unsigned int i = 0; i < numValues; ++i)
        h_values[i] = rand() & 1;

    // Allocate device memory for the values
    int* d_values;
    CUDA_SAFE_CALL(cudaMalloc((void**)&d_values, size));

    // Copy values from host memory to device memory
    CUDA_SAFE_CALL(cudaMemcpy(d_values, h_values, size, cudaMemcpyHostToDevice));

    // Create timer
    unsigned int timer;
    CUT_SAFE_CALL(cutCreateTimer(&timer));
    
    // Allocate memory used during the reduction operation
    reduceAllocate();
    
    // Device memory pointer to the sum of all values
    int* d_result;

    // Start timer to time reduction on the device
    CUT_SAFE_CALL(cutResetTimer(timer));
    CUT_SAFE_CALL(cutStartTimer(timer));

    // Loop multiple times to get accurate timing
    int numIter = 100;
    for (int i = 0; i < numIter; ++i) {
    
        // Perform reduction on the device
        d_result = reduce(d_values, numValues);

    }
    
    // Synchronize to make sure the device is done computing
    CUDA_SAFE_CALL(cudaThreadSynchronize());

    // Stop timer
    CUT_SAFE_CALL(cutStopTimer(timer));

    // Print average execution
    float time = cutGetTimerValue(timer) / numIter;
    float bandwidth = 1e-6f * (numValues * sizeof(int)) / time;
    printf("Average time is %f ms for %d values\n", time, numValues);
    printf("So average bandwidth is %f GB/s\n", bandwidth);

    // Reduce on the host
    int resultHost = reduceHost(h_values, numValues);
    
    // Retrieve result of reduction on the device
    int resultDevice;
    CUDA_SAFE_CALL(cudaMemcpy(&resultDevice, d_result, sizeof(int), cudaMemcpyDeviceToHost));
    
    // Check host result against device result
    if (resultHost == resultDevice)
        printf("Test PASSED\n");
    else
        printf("Test FAILED (%d (device) != %d (host))\n", resultDevice, resultHost);

    // Free memory used during the reduction operation
    reduceFree();

    // Free memory
    CUDA_SAFE_CALL(cudaFree(d_values));
    free(h_values);

    // Prompt for exit
   // if (argc == 1)
    //    CUT_EXIT(argc, argv);
}

////////////////////////////////////////////////////////////////////////////////
//! Reduction on the host
//! @param values       input array of values to be reduced
//! @param numValues    number of values to be reduced
////////////////////////////////////////////////////////////////////////////////
int reduceHost(const int* values, unsigned int numValues)
{
    int result = 0;
    for (unsigned int i = 0; i < numValues; ++i)
        result += values[i];
    return result;
}
