###Computación paralela-CUDA
######Samuel Alfageme Saiz
######Pablo García Sanz

Fórmula para el identificador global de cada thread

```
int idGlobal = threadIdx.x + (blockIdx.x * blockDim.x) + 
				((gridDim.x * blockDim.x) * threadIdx.y + 
				(blockIdx.y * blockDim.y));
```
Máximo del tamaño del grid en la cola **cudaslurm**
```
 2147483647
 65535
 65535
```
