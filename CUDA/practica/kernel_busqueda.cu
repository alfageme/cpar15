/********************************************************
         Computación Paralela - Curso 2014/2015
    
          Práctica5 : Búsqueda de Cadenas CUDA
    
    @author Samuel Alfageme, Pablo García - grupo 35
*********************************************************/

/*
 * Función de búsqueda del device
 */
__global__ void gpu_FuncBusqueda(const int *B, int sizeB, const int *S, int sizeS, int *resultado)
{
    // Porción de memoria shared entre todos los SP de un SM
    //extern __shared__ int dB[];

    // Identificador global del hilo en el grid
    unsigned int startB = threadIdx.x + blockDim.x * blockIdx.x;

    // Cada hilo carga un elemento de la memoria global a la memoria compartida:
    //dB[threadIdx.x] = B[startB];
    //__syncthreads();

    if(startB < *resultado && startB <= sizeB - sizeS){

        int i;
        int ind = 0;
        int flag = 1;

        for(i=startB; ind<sizeS && i<sizeB ; ind++, i++){
            if(S[ind] != B[i]){
                flag = 0;
                break;
            }
        }

        if(flag)
            atomicMin(resultado, startB);
    }
}